# File Delete UI

This module adds the capability to delete files from the file admin view (UI).
It assumes that the admin knows what they are doing and trusts that when they want to delete a file (even one with a non zero usage count) that they really want to remove the file.
When removing files that are referenced by other entities, core takes care of removing the reference.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/file_delete_ui).

 * To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/file_delete_ui).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

No configuration is needed.


## Maintainers

- Doug Green - [douggreen](https://www.drupal.org/u/douggreen)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
